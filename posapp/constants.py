VENDOR_STATUS = (
    ('Pending', 'Pending'),
    ('Approved', 'Approved'),
    ('Rejected', 'Rejected'),
    ('Black Listed', 'Black Listed'),
)

PRODUCT_STATUS = (
    ('Pending', 'Pending'),
    ('Approved', 'Approved'),
    ('Rejected', 'Rejected')
)

SIZE = (
    (None, "Select Standard"),
    ("Int", "Int"),
    ("EU", "EU"),
)


GENDER = (
    ('male', 'male'),
    ('female', 'female'),
    ('unisex', 'unisex'),
)

AGE_GROUP = (
    ("adult", "adult"),
    ("all ages", "all ages"),
    ("teen", "teen"),
    ("kids", "kids"),
    ("toddler", "toddler"),
    ("infant", "infant"),
    ("newborn", "newborn"),
)

SHOW_IN = (
    ("POS", "POS"),
    ("Ecommerce and POS", "Ecommerce and POS"),
)

YEARS = ((1995 + i, 1995 + i) for i in range(26))
