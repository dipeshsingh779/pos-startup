# Generated by Django 3.2.4 on 2021-07-18 07:08

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(max_length=200, unique=True)),
                ('slug', models.SlugField(blank=True, null=True, unique=True)),
                ('default_city', models.BooleanField(default=False)),
                ('logistic_charge', models.PositiveIntegerField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CityCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(max_length=200)),
                ('default_shipping_charge', models.PositiveIntegerField(default=50)),
                ('additional_shipping_cost_per_product', models.PositiveIntegerField(default=25)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(default='No Color', max_length=200)),
                ('hex_code', models.CharField(blank=True, max_length=200, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(max_length=200)),
                ('slug', models.SlugField(blank=True, null=True, unique=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PosCustomer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('name', models.CharField(max_length=200)),
                ('mobile', models.CharField(max_length=30)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('street_address', models.CharField(blank=True, max_length=100, null=True)),
                ('city', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.city')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PosPurchase',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('subtotal', models.PositiveIntegerField(default=0)),
                ('discount_amount', models.PositiveIntegerField(default=0)),
                ('total', models.PositiveIntegerField(default=0)),
                ('vat_pct', models.PositiveIntegerField(default=0)),
                ('vat_amount', models.PositiveIntegerField(default=0)),
                ('nettotal', models.PositiveIntegerField(default=0)),
                ('purchase_date', models.DateTimeField()),
                ('bill_no', models.CharField(max_length=50)),
                ('advance_payment', models.PositiveIntegerField(default=0)),
                ('remaining_amount', models.PositiveIntegerField(default=0)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('is_canceled', models.BooleanField(blank=True, default=False, null=True)),
                ('canceled_date', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PosSale',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('subtotal', models.PositiveIntegerField(default=0)),
                ('discount_amount', models.PositiveIntegerField(default=0)),
                ('vat_amount', models.PositiveIntegerField(default=0)),
                ('total', models.PositiveIntegerField(default=0)),
                ('shipping_charge', models.PositiveIntegerField(default=0)),
                ('nettotal', models.PositiveIntegerField(default=0)),
                ('sale_date', models.DateTimeField()),
                ('is_complete', models.BooleanField(default=False)),
                ('completed_date', models.DateTimeField(blank=True, null=True)),
                ('customer_payment_status', models.BooleanField(default=False)),
                ('paid_date', models.DateTimeField(blank=True, null=True)),
                ('is_canceled', models.BooleanField(default=False)),
                ('canceled_date', models.DateTimeField(blank=True, null=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('customer', models.ForeignKey(default=1, on_delete=django.db.models.deletion.SET_DEFAULT, to='posapp.poscustomer')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(max_length=200)),
                ('product_id', models.CharField(blank=True, max_length=200, null=True, unique=True)),
                ('description', models.TextField()),
                ('cart_limit', models.PositiveIntegerField(default=10)),
                ('video_url', models.CharField(blank=True, max_length=200, null=True)),
                ('commission_pct', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=4, null=True)),
                ('status', models.CharField(choices=[('Pending', 'Pending'), ('Approved', 'Approved'), ('Rejected', 'Rejected')], default='Pending', max_length=100)),
                ('remarks', models.CharField(blank=True, max_length=200, null=True)),
                ('is_featured', models.BooleanField(default=False)),
                ('vendor_is_featured', models.BooleanField(blank=True, default=False, null=True)),
                ('avg_rating', models.DecimalField(decimal_places=2, default=0, max_digits=3)),
                ('rating_count', models.PositiveIntegerField(default=0)),
                ('sale_count', models.PositiveIntegerField(default=0)),
                ('length', models.PositiveIntegerField(default=0)),
                ('width', models.PositiveIntegerField(default=0)),
                ('height', models.PositiveIntegerField(default=0)),
                ('weight', models.DecimalField(decimal_places=2, default=0, max_digits=19)),
                ('return_policy', models.TextField(blank=True, max_length=1024, null=True)),
                ('gender', models.CharField(blank=True, choices=[('male', 'male'), ('female', 'female'), ('unisex', 'unisex')], default='unisex', max_length=50, null=True)),
                ('age_group', models.CharField(blank=True, choices=[('adult', 'adult'), ('all ages', 'all ages'), ('teen', 'teen'), ('kids', 'kids'), ('toddler', 'toddler'), ('infant', 'infant'), ('newborn', 'newborn')], default='adult', max_length=50, null=True)),
                ('show_in', models.CharField(choices=[('POS', 'POS'), ('Ecommerce and POS', 'Ecommerce and POS')], default='Ecommerce and POS', max_length=20)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProductBrand',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(max_length=200, unique=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='productbrands')),
                ('description', models.TextField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProductSku',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('sku', models.CharField(blank=True, max_length=50, null=True)),
                ('seller_sku', models.CharField(blank=True, max_length=50, null=True)),
                ('instock', models.PositiveBigIntegerField(default=0)),
                ('views', models.PositiveIntegerField(default=0)),
                ('marked_price', models.DecimalField(decimal_places=2, max_digits=19)),
                ('selling_price', models.DecimalField(decimal_places=2, max_digits=19)),
                ('discount_pct', models.PositiveIntegerField(default=0)),
                ('flash_sale_price', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('status', models.CharField(choices=[('Pending', 'Pending'), ('Approved', 'Approved'), ('Rejected', 'Rejected')], default='Pending', max_length=100)),
                ('featured_productsku', models.BooleanField(default=False)),
                ('color', models.ForeignKey(blank=True, default=8, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='posapp.color')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.product')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProductStock',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('instock', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ['outlet__id'],
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(max_length=200)),
                ('slug', models.CharField(blank=True, max_length=200, null=True, unique=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(blank=True, max_length=200, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('name', models.CharField(max_length=200)),
                ('image', models.ImageField(blank=True, null=True, upload_to='user/vendor')),
                ('vendor_status', models.CharField(choices=[('Pending', 'Pending'), ('Approved', 'Approved'), ('Rejected', 'Rejected'), ('Black Listed', 'Black Listed')], default='Pending', max_length=50)),
                ('shop_name', models.CharField(default='Shop', max_length=50)),
                ('estd', models.PositiveIntegerField(choices=[(1995, 1995), (1996, 1996), (1997, 1997), (1998, 1998), (1999, 1999), (2000, 2000), (2001, 2001), (2002, 2002), (2003, 2003), (2004, 2004), (2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020)], default=2010)),
                ('description', models.TextField(blank=True, null=True)),
                ('street_address', models.CharField(default='Sundar Bajar, Koteshwar-25', max_length=200)),
                ('shop_type', models.CharField(default='fashion store', max_length=200)),
                ('contact', models.CharField(default='9898989898', max_length=200)),
                ('alt_contact', models.CharField(blank=True, max_length=200, null=True)),
                ('registration_document', models.FileField(blank=True, null=True, upload_to='user/vendor/docs/')),
                ('vat_pan_document', models.FileField(blank=True, null=True, upload_to='user/vendor/docs/')),
                ('other_document', models.FileField(blank=True, null=True, upload_to='user/vendor/docs/')),
                ('eligible_for_pos', models.BooleanField(blank=True, default=False, null=True)),
                ('city', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.city')),
                ('district', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.district')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='VendorOutlet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('outlet', models.CharField(max_length=200)),
                ('contact', models.CharField(blank=True, max_length=200, null=True)),
                ('street_address', models.CharField(max_length=200)),
                ('is_main', models.BooleanField(blank=True, default=False, null=True)),
                ('city', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.city')),
                ('operator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.vendor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Supplier',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('supplier_name', models.CharField(max_length=200)),
                ('contact_person', models.CharField(max_length=200)),
                ('contact', models.CharField(max_length=50)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('address', models.CharField(blank=True, max_length=500, null=True)),
                ('opening_balance', models.DecimalField(decimal_places=2, default=0, max_digits=19)),
                ('closing_balance', models.DecimalField(decimal_places=2, default=0, max_digits=19)),
                ('description', models.TextField(blank=True, null=True)),
                ('outlet', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='posapp.vendoroutlet')),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.vendor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StockTransfer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('quantity', models.PositiveIntegerField(default=1)),
                ('status', models.CharField(blank=True, choices=[('Pending', 'Pending'), ('Approved', 'Approved'), ('Rejected', 'Rejected')], default='Pending', max_length=200, null=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('product_sku', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='posapp.productsku')),
                ('request_sender', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fromstocktransfers', to='posapp.vendoroutlet')),
                ('requested_to', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tostocktransfers', to='posapp.vendoroutlet')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StockAdjust',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('initial_stock', models.IntegerField()),
                ('quantity', models.IntegerField()),
                ('note', models.TextField(default='Stock Adjusted Because: ')),
                ('action', models.CharField(choices=[('Increased', 'Increased'), ('Decreased', 'Decreased')], max_length=200)),
                ('product_stock', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.productstock')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Size',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('standard', models.CharField(choices=[(None, 'Select Standard'), ('Int', 'Int'), ('EU', 'EU')], max_length=200)),
                ('title', models.CharField(default='No Size', max_length=200)),
            ],
            options={
                'unique_together': {('standard', 'title')},
            },
        ),
        migrations.CreateModel(
            name='SaleActivity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('activity', models.TextField()),
                ('remarks_or_activity', models.CharField(choices=[('Activity', 'Activity'), ('Remarks', 'Remarks')], default='Activity', max_length=20)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('outlet', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.vendoroutlet')),
                ('sale', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.possale')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PurchaseActivity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('activity', models.TextField()),
                ('remarks_or_activity', models.CharField(choices=[('Activity', 'Activity'), ('Remarks', 'Remarks')], default='Activity', max_length=20)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('outlet', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.vendoroutlet')),
                ('purchase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.pospurchase')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='productstock',
            name='outlet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.vendoroutlet'),
        ),
        migrations.AddField(
            model_name='productstock',
            name='product_sku',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.productsku'),
        ),
        migrations.AddField(
            model_name='productsku',
            name='size',
            field=models.ForeignKey(blank=True, default=8, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='posapp.size'),
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='products')),
                ('featured_image', models.BooleanField(blank=True, default=False, null=True)),
                ('productsku', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='posapp.productsku')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProductCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('title', models.CharField(max_length=200)),
                ('image', models.ImageField(blank=True, null=True, upload_to='productcategory')),
                ('show_in_homepage', models.BooleanField(default=True)),
                ('shipping_criticalness', models.DecimalField(decimal_places=1, default=1, max_digits=2)),
                ('google_category', models.CharField(blank=True, max_length=1024, null=True)),
                ('sync_to_facebook', models.BooleanField(blank=True, default=True, null=True)),
                ('root', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='posapp.productcategory')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='product',
            name='brand',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.productbrand'),
        ),
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.productcategory'),
        ),
        migrations.AddField(
            model_name='product',
            name='seller',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.vendor'),
        ),
        migrations.AddField(
            model_name='product',
            name='tags',
            field=models.ManyToManyField(to='posapp.Tag'),
        ),
        migrations.CreateModel(
            name='PosSaleProduct',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('rate', models.PositiveIntegerField()),
                ('quantity', models.PositiveIntegerField()),
                ('subtotal', models.PositiveIntegerField()),
                ('pos_sale', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.possale')),
                ('product_sku', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.productsku')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='possale',
            name='outlet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.vendoroutlet'),
        ),
        migrations.CreateModel(
            name='PosPurchaseProduct',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('rate', models.PositiveIntegerField()),
                ('quantity', models.PositiveIntegerField()),
                ('subtotal', models.PositiveIntegerField()),
                ('pos_purchase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.pospurchase')),
                ('product_sku', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.productsku')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PosPurchasePayment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('amount', models.IntegerField()),
                ('payment_date', models.DateTimeField()),
                ('payment_group_id', models.CharField(blank=True, max_length=200, null=True)),
                ('pos_purchase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.pospurchase')),
                ('supplier', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.supplier')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='pospurchase',
            name='outlet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.vendoroutlet'),
        ),
        migrations.AddField(
            model_name='pospurchase',
            name='supplier',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.supplier'),
        ),
        migrations.AddField(
            model_name='poscustomer',
            name='outlet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.vendoroutlet'),
        ),
        migrations.CreateModel(
            name='PosActivity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('object_id', models.PositiveIntegerField()),
                ('activity', models.TextField()),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('object_model', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype')),
                ('outlet', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.vendoroutlet')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='district',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.state'),
        ),
        migrations.AddField(
            model_name='city',
            name='city_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='posapp.citycategory'),
        ),
        migrations.AddField(
            model_name='city',
            name='district',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posapp.district'),
        ),
    ]
