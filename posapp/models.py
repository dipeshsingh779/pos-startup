from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User, Group
from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save
from .constants import VENDOR_STATUS, PRODUCT_STATUS, SIZE, GENDER, AGE_GROUP, SHOW_IN, YEARS
from .utils import unique_slug_generator





#TimeStamp

class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        return super().save(*args, **kwargs)



#state Details

class State(TimeStamp):
    title = models.CharField(max_length=200)
    slug = models.CharField(unique=True, max_length=200, null=True, blank=True)

    def __str__(self):
        return self.title




#District Details

class District(TimeStamp):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, null=True, blank=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


#City Category Details

class CityCategory(TimeStamp):
    title = models.CharField(max_length=200)
    default_shipping_charge = models.PositiveIntegerField(default=50)
    additional_shipping_cost_per_product = models.PositiveIntegerField(
        default=25)

    def __str__(self):
        return self.title



#City Deatils

class City(TimeStamp):
    title = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(unique=True, null=True, blank=True)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    city_category = models.ForeignKey(
        CityCategory, on_delete=models.SET_NULL, null=True, blank=True)
    default_city = models.BooleanField(default=False)
    logistic_charge = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.title



#Vendor Details
class Vendor(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    image = models.ImageField(
        upload_to='user/vendor', null=True, blank=True)
    vendor_status = models.CharField(
        max_length=50, choices=VENDOR_STATUS, default="Pending")
    shop_name = models.CharField(max_length=50, default="Shop")
    estd = models.PositiveIntegerField(default=2010, choices=YEARS)
    description = models.TextField(null=True, blank=True)
    district = models.ForeignKey(
        District, on_delete=models.SET_NULL, null=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True)
    street_address = models.CharField(
        max_length=200, default="Sundar Bajar, Koteshwar-25")
    shop_type = models.CharField(max_length=200, default="fashion store")
    contact = models.CharField(max_length=200, default="9898989898")
    alt_contact = models.CharField(max_length=200, null=True, blank=True)
    registration_document = models.FileField(
        upload_to="user/vendor/docs/", null=True, blank=True)
    vat_pan_document = models.FileField(
        upload_to="user/vendor/docs/", null=True, blank=True)
    other_document = models.FileField(
        upload_to="user/vendor/docs/", null=True, blank=True)
    eligible_for_pos = models.BooleanField(
        default=False, null=True, blank=True)

    def __str__(self):
        return self.shop_name + "(" + self.name + ")"

    def save(self, *args, **kwargs):
        group, group_created = Group.objects.get_or_create(name="Vendor")
        self.user.groups.add(group)
        super().save(*args, **kwargs)




#Vendor Outlet

class VendorOutlet(TimeStamp):
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    outlet = models.CharField(max_length=200)
    operator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True) # leave null for main outlet
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True)
    contact = models.CharField(max_length=200, null=True, blank=True)
    street_address = models.CharField(max_length=200)
    is_main = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.outlet + "(" + self.vendor.shop_name + ")"



#Supplier Details
class Supplier(TimeStamp):
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    supplier_name = models.CharField(max_length=200)
    contact_person = models.CharField(max_length=200)
    contact = models.CharField(max_length=50)
    email = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=500, null=True, blank=True)
    opening_balance = models.DecimalField(
        decimal_places=2, max_digits=19, default=0)
    closing_balance = models.DecimalField(
        decimal_places=2, max_digits=19, default=0)
    description = models.TextField(null=True, blank=True)
    outlet = models.ForeignKey(VendorOutlet, on_delete=models.CASCADE, null=True, blank=True)
    def __str__(self):
        return self.supplier_name



# Customers


class PosCustomer(TimeStamp):
    outlet = models.ForeignKey(VendorOutlet, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    city = models.ForeignKey(
        City, on_delete=models.SET_NULL, null=True, blank=True)
    mobile = models.CharField(max_length=30)
    email = models.EmailField(null=True, blank=True)
    street_address = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name




#Color Details
class Color(TimeStamp):
    title = models.CharField(max_length=200, default='No Color')
    hex_code = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.title


#size Details
class Size(TimeStamp):
    standard = models.CharField(max_length=200, choices=SIZE)
    title = models.CharField(max_length=200, default='No Size')

    class Meta:
        unique_together = ('standard', 'title')

    def __str__(self):
        return self.standard + " - " + self.title



#Product Category Details
class ProductCategory(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(
        upload_to="productcategory", null=True, blank=True)
    root = models.ForeignKey("self", on_delete=models.CASCADE,
                             null=True, blank=True)
    show_in_homepage = models.BooleanField(default=True)
    shipping_criticalness = models.DecimalField(
        default=1, max_digits=2, decimal_places=1)
    google_category = models.CharField(max_length=1024, null=True, blank=True)
    sync_to_facebook = models.BooleanField(default=True, null=True, blank=True)

    # objects = models.Manager()
    # shown_objects = ShownInHomepage()

    def __str__(self):
        return self.title

    def show_on_home(self):
        return self.productcategory_set.filter(show_in_homepage=True)

    def home_products(self):
        return self.product_set.filter(is_active=True).order_by('?')[:6]


#Tag Details
class Tag(TimeStamp):
    title = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.title


#Product Brand Details
class ProductBrand(TimeStamp):
    title = models.CharField(max_length=200, unique=True)
    image = models.ImageField(
        upload_to="productbrands", null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title

#Product Details
class Product(TimeStamp):
    title = models.CharField(max_length=200)
    product_id = models.CharField(
        max_length=200, unique=True, null=True, blank=True)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
    brand = models.ForeignKey(ProductBrand, on_delete=models.CASCADE)
    seller = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    description = models.TextField()
    cart_limit = models.PositiveIntegerField(default=10)
    video_url = models.CharField(max_length=200, null=True, blank=True)
    commission_pct = models.DecimalField(
        max_digits=4, decimal_places=2, default=0, null=True, blank=True)
    status = models.CharField(
        max_length=100, choices=PRODUCT_STATUS, default="Pending")
    remarks = models.CharField(max_length=200, null=True, blank=True)
    is_featured = models.BooleanField(default=False)
    vendor_is_featured = models.BooleanField(
        default=False, null=True, blank=True)
    avg_rating = models.DecimalField(default=0, max_digits=3, decimal_places=2)
    rating_count = models.PositiveIntegerField(default=0)
    sale_count = models.PositiveIntegerField(default=0)
    # weight and volumes
    length = models.PositiveIntegerField(default=0)
    width = models.PositiveIntegerField(default=0)
    height = models.PositiveIntegerField(default=0)
    weight = models.DecimalField(max_digits=19, decimal_places=2, default=0)
    return_policy = models.TextField(max_length=1024, null=True, blank=True)
    gender = models.CharField(
        max_length=50, choices=GENDER, default="unisex", null=True, blank=True)
    age_group = models.CharField(
        max_length=50, choices=AGE_GROUP, default="adult", null=True, blank=True)

    show_in = models.CharField(max_length=20, default="Ecommerce and POS", choices=SHOW_IN)


#ProductSKU Details

class ProductSkuManager(models.Manager):
    def myfirst(self, pk):
        return ProductSku.objects.filter(is_active=True, status="Approved")[0]

class ProductSku(TimeStamp):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    sku = models.CharField(max_length=50, null=True, blank=True)
    seller_sku = models.CharField(max_length=50, null=True, blank=True)
    instock = models.PositiveBigIntegerField(default=0)
    views = models.PositiveIntegerField(default=0)
    color = models.ForeignKey(
        Color, on_delete=models.SET_DEFAULT, default=8, null=True, blank=True)
    size = models.ForeignKey(
        Size, on_delete=models.SET_DEFAULT, default=8, null=True, blank=True)
    marked_price = models.DecimalField(max_digits=19, decimal_places=2)
    selling_price = models.DecimalField(max_digits=19, decimal_places=2)
    discount_pct = models.PositiveIntegerField(default=0)
    flash_sale_price = models.DecimalField(
        max_digits=19, decimal_places=2, null=True, blank=True)
    status = models.CharField(
        max_length=100, choices=PRODUCT_STATUS, default="Pending")
    featured_productsku = models.BooleanField(default=False)

    # objects = ProductSkuManager()

    objects = ProductSkuManager()


#Product Stock
class ProductStock(TimeStamp):
    product_sku = models.ForeignKey(ProductSku, on_delete=models.CASCADE)
    outlet = models.ForeignKey(VendorOutlet, on_delete=models.CASCADE)
    instock = models.IntegerField(default=0)

    class Meta:
        ordering = ["outlet__id"]

    def __str__(self):
        return self.product_sku.sku + " - " + str(self.instock)



Stock_Adjust_Action = (
    ('Increased', 'Increased'),
    ('Decreased', 'Decreased')
)
class StockAdjust(TimeStamp):
    product_stock = models.ForeignKey(ProductStock, on_delete=models.CASCADE)
    initial_stock = models.IntegerField()
    quantity = models.IntegerField()
    note = models.TextField(default='Stock Adjusted Because: ')
    action = models.CharField(max_length=200, choices=Stock_Adjust_Action)

    def __str__(self):
        return str(self.product_stock.product_sku.id) + self.action + str(self.quantity)


#Product Image
class ProductImage(TimeStamp):
    productsku = models.ForeignKey(ProductSku, on_delete=models.CASCADE, null=True, blank=True)
    image = models.ImageField(upload_to="products", null=True, blank=True)
    featured_image = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return self.productsku.product.title


StockTransferStatus = (
    ('Pending', 'Pending'),
    ('Approved', 'Approved'),
    ('Rejected', 'Rejected'),
)

#Product Transfer
class StockTransfer(TimeStamp):
    product_sku = models.ForeignKey(ProductSku, on_delete=models.CASCADE, null=True, blank=True)
    request_sender = models.ForeignKey(VendorOutlet, on_delete=models.CASCADE, related_name="fromstocktransfers", null=True, blank=True)
    requested_to = models.ForeignKey(VendorOutlet, on_delete=models.CASCADE, related_name="tostocktransfers", null=True, blank=True)
    quantity = models.PositiveIntegerField(default=1)
    status = models.CharField(max_length=200, default='Pending', choices=StockTransferStatus, null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.request_sender.outlet

    @property
    def activity_title(self):
        return 'Stock Transfer' 



# puchasing


class PosPurchase(TimeStamp):
    outlet = models.ForeignKey(VendorOutlet, on_delete=models.CASCADE)
    supplier = models.ForeignKey(
        Supplier, on_delete=models.SET_NULL, null=True, blank=True)
    subtotal = models.PositiveIntegerField(default=0)
    discount_amount = models.PositiveIntegerField(default=0)
    total = models.PositiveIntegerField(default=0)
    vat_pct = models.PositiveIntegerField(default=0)
    vat_amount = models.PositiveIntegerField(default=0)
    nettotal = models.PositiveIntegerField(default=0)
    purchase_date = models.DateTimeField()
    bill_no = models.CharField(max_length=50)
    advance_payment = models.PositiveIntegerField(default=0)
    remaining_amount = models.PositiveIntegerField(default=0)
    remarks = models.TextField(null=True, blank=True)
    is_canceled = models.BooleanField(default=False, null=True, blank=True)
    canceled_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.bill_no


class PosPurchaseProduct(TimeStamp):
    pos_purchase = models.ForeignKey(PosPurchase, on_delete=models.CASCADE)
    product_sku = models.ForeignKey(
        ProductSku, on_delete=models.SET_NULL, null=True, blank=True)
    rate = models.PositiveIntegerField()
    quantity = models.PositiveIntegerField()
    subtotal = models.PositiveIntegerField()

    def __str__(self):
        return "Purchase_" + str(self.pos_purchase.id) + " ppp_" + str(self.id)

    @property
    def activity_title(self):
        return 'Purchase (Stock In)'


class PosPurchasePayment(TimeStamp):
    pos_purchase = models.ForeignKey(PosPurchase, on_delete=models.CASCADE)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    amount = models.IntegerField()
    payment_date = models.DateTimeField()
    payment_group_id = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return "Purchase_" + self.pos_purchase.bill_no + " amount:" + str(self.amount)


# sales

class PosSale(TimeStamp):
    outlet = models.ForeignKey(VendorOutlet, on_delete=models.CASCADE)
    customer = models.ForeignKey(PosCustomer, on_delete=models.SET_DEFAULT, default=1)
    subtotal = models.PositiveIntegerField(default=0)
    discount_amount = models.PositiveIntegerField(default=0)
    vat_amount = models.PositiveIntegerField(default=0)
    total = models.PositiveIntegerField(default=0)
    shipping_charge = models.PositiveIntegerField(default=0)
    nettotal = models.PositiveIntegerField(default=0)
    sale_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)
    completed_date = models.DateTimeField(null=True, blank=True)
    customer_payment_status = models.BooleanField(default=False)
    paid_date = models.DateTimeField(null=True, blank=True)
    is_canceled = models.BooleanField(default=False)
    canceled_date = models.DateTimeField(null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)

    def __str__(self):
        return "Sale: " + str(self.id)


class PosSaleProduct(TimeStamp):
    pos_sale = models.ForeignKey(PosSale, on_delete=models.CASCADE)
    product_sku = models.ForeignKey(
        ProductSku, on_delete=models.SET_NULL, null=True, blank=True)
    rate = models.PositiveIntegerField()
    quantity = models.PositiveIntegerField()
    subtotal = models.PositiveIntegerField()

    def __str__(self):
        return "Sale_" + str(self.pos_sale.id) + " psp_" + str(self.id)

    @property
    def activity_title(self):
        return 'Sales (Stock Out)'

A_OR_R = (
    ("Activity", "Activity"), ("Remarks", "Remarks")
)

class PurchaseActivity(TimeStamp):
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    purchase = models.ForeignKey(PosPurchase, on_delete=models.CASCADE)
    outlet = models.ForeignKey(
        VendorOutlet, on_delete=models.SET_NULL, null=True, blank=True)
    activity = models.TextField()
    remarks_or_activity = models.CharField(
        max_length=20, default="Activity", choices=A_OR_R)

    def __str__(self):
        return "Activity: Purchase_" + str(self.purchase.id) + "(" + self.activity + ")"


class SaleActivity(TimeStamp):
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    sale = models.ForeignKey(PosSale, on_delete=models.CASCADE)
    outlet = models.ForeignKey(
        VendorOutlet, on_delete=models.SET_NULL, null=True, blank=True)
    activity = models.TextField()
    remarks_or_activity = models.CharField(
        max_length=20, default="Activity", choices=A_OR_R)

    def __str__(self):
        return "Activity: Sale_" + str(self.sale.id) + "(" + self.activity + ")"


class PosActivity(TimeStamp):
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    object_model = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    object = GenericForeignKey('object_model', 'object_id')
    activity = models.TextField()
    outlet = models.ForeignKey(VendorOutlet, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return str(self.created_at) + ": " + self.activity


def all_pre_save(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(all_pre_save, sender=State)
pre_save.connect(all_pre_save, sender=District)
pre_save.connect(all_pre_save, sender=City)